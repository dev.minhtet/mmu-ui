import Vue from 'vue'
import './plugins/vuetify'
import App from './App.vue'
import store from './store'
import router1 from './router'

Vue.config.productionTip = false

new Vue({
  router: router1,
  store,
  render: h => h(App),
}).$mount('#app')
