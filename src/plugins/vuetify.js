import Vue from 'vue'
import Vuetify from 'vuetify/lib'
import Vuex from 'vuex'
import VueRouter from 'vue-router'
import 'vuetify/src/stylus/app.styl'
import 'es6-promise/auto'


Vue.use(VueRouter)
Vue.use(Vuetify, {
  iconfont: 'md',
})
Vue.use(Vuex)
