import VueRouter from 'vue-router'
import student from '../components/Student'
import newStudent from '../components/NewStudent'
import home from '../components/Home'
import subject from '../components/Subject'
import newSubject from '../components/NewSubject'
import newMajor from '../components/NewMajor'
import major from '../components/Major'

let router = new VueRouter({
  mode: 'history',
    routes: [
      {
        path: '/student',
        component: student,
        name: 'student'
      },
      {
        path: '/student/new',
        component: newStudent,
        name: 'newStudent'
      }
      ,
      {
        path: '/home',
        component: home,
        name: 'home'
      },
      {
        path: '/subject',
        component: subject,
        name: 'subject'
      },
      {
        path: '/subject/new',
        component: newSubject,
        name: 'newSubject'
      },
      {
        path: '/major',
        component: major,
        name: 'major'
      },
      {
        path: '/major/new',
        component: newMajor,
        name: 'newMajor'
      },
    ]
  })

export default router;