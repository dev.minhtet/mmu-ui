import routes from './routes';
import axios from 'axios';

const getAll = () => axios.get(routes.getAllMajors).then((response) => {
  return response.data;
}).catch((error) => {
  console.log(error);
});

const save = (data) => axios.post(routes.saveMajors,data,{
    headers: {
        'Content-Type': 'application/json'
    }
}).then((response) => {
    console.log('major saved');
    return response.data;
  }).catch((error) => {
    console.log("-- Error --");
    console.log("url: "+routes.saveMajors)
    console.log(error);
  });

export default{
  getAll,
  save
};
