const baseUrl = 'http://003a9c40.ngrok.io/';
const baseUrl2 = 'http://192.168.0.108:8000'
const routes = {
  getAllStudents: `${baseUrl}students`,
  saveStudent: `${baseUrl}students`,
  getAllSubjects : `${baseUrl}subjects`,
  saveSubject: `${baseUrl}subject`,
  getAllMajors: `${baseUrl}majors`,
  saveMajors: `${baseUrl}majors`,
};

export default routes;
