import routes from './routes';
import axios from 'axios';

const getAll = () => axios.get(routes.getAllStudents).then((response) => {
  return response.data;
}).catch((error) => {
  console.log(error);
});

const save = (data) => axios.post(routes.saveStudent,data,{
    headers: {
        'Content-Type': 'application/json'
    }
}).then((response) => {
    console.log('student saved');
    return response.data;
  }).catch((error) => {
    console.log("-- Error --");
    console.log(error);
  });

export default{
  getAll,
  save
};
