import axios from 'axios';
import routes from './routes';

const getAll = () => axios.get(routes.getAllSubjects).then((response) => {
    return response.data;
  }).catch((error) => {
    console.log(error);
  });
  
  const save = (data) => axios.post(routes.saveSubject,data,{
      headers: {
          'Content-Type': 'application/json'
      }
  }).then((response) => {
      console.log('subject saved');
      return response.data;
    }).catch((error) => {
      console.log("-- Error --");
      console.log(error);
    });

    
export default{
  getAll,
  save
};
