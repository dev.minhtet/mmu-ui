import Vuex from 'vuex'

const store = new Vuex.Store({
    state: {
      count: 1
    },
    mutations: {
      increment (state) {
        state.count++
      }
    }
  })

export default store;